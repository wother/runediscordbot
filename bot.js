import dotenv from 'dotenv';
dotenv.config();

// Workers imports
import { runeToMessage } from "./workers/runeToEmbed.js";
import { parseMessage } from "./workers/commandStringParse.js";

// API Import
import { setupAPI } from "./api/app.js";

// Import libraries
import { Client, Presence, Intents } from 'discord.js';
const client = new Client({
  intents: [
    Intents.FLAGS.GUILDS,
    Intents.FLAGS.GUILD_MESSAGES,
    Intents.FLAGS.GUILD_PRESENCES
  ]
});

client.on("ready", () => {
  console.log(`Logged in as ${client.user.tag}!`);

  const presenceObj = {
    activities: [
      { name: '!help for commands' , type: 'LISTENING' }
    ],
    status: 'active'
  };
  client.user.setPresence(presenceObj);
});

// Event listener when a user sends a message in the chat.
client.on("messageCreate", msg => {
  // We check the message content and parse it
  const parsedMessage = parseMessage(msg.content);

  if (parsedMessage && parsedMessage.type === "text") {
    msg.channel.send(parsedMessage.content);
  } else if (parsedMessage && parsedMessage.type === "embed") {
    msg.channel.send(runeToMessage(parsedMessage.content));
  } else if (parsedMessage && parsedMessage.type === "runeArray") {
    // Default Rune array
    parsedMessage.content.forEach(runeObj => {
      msg.channel.send(runeToMessage(runeObj));
    });
  } else if (parsedMessage && parsedMessage.type === "allRunesLinks") {
    // All rune array
    msg.channel.send({
      content: "Here are links to descriptions and proper spelling for all the runes.",
      components: parsedMessage.content.components
    });
  }
});

// Initialize bot by connecting to the server
client.login(process.env.DISCORD_TOKEN);
setupAPI();